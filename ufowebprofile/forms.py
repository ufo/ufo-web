from django import forms
from django.utils.translation import ugettext_lazy as _
from django import forms

class EditProfileForm(forms.Form):
    """
    Form for editing a user account.
    """
    email = forms.CharField(max_length=75, label=_("Email address"))
    first = forms.CharField(max_length=64, label=_("First name"), required=True)
    last = forms.CharField(max_length=64, label=_("Last name"), required=True)
    avatar = forms.FileField(required=False)
