{% load i18n %}

var notifications_json = null;

/* Notification box (toastmessage) */

function _notify(type, message, options)
{
	if (!options) {
		options = {};
	}
	options.text = message;
	options.type = type;
	return $().toastmessage('showToast', options);
}

function notify_success(message, options) {
	return _notify('success', message, options);
}

function notify(message, options) {
	return _notify('notice', message, options);
}

function notify_warning(message, options) {
	return _notify('warning', message, options);
}

function notify_error(message, options) {
	return _notify('error', message, options);
}

Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};

$.PeriodicalUpdater( {
	url: '{% url notifications %}',
	method: 'get',
	minTimeout: 15000,
	maxTimeout: 60000,
	type: 'json'
}, function(data) {
	if (notifications_json) {
		var old_nb = notifications_json.length;
	}
	else {
		var old_nb = 0;
	}
	notifications_json = data.notifications;
	var new_nb = notifications_json.length;
	refresh_notifications_count();
	if (old_nb != -1 && old_nb < new_nb) {
		for (var i = 0; i < notifications_json.length; i++) {
			var div = $("<div>");
			$("#notification-template").tmpl(notifications_json[i]).appendTo(div);
			notify(div, {
				notification : notifications_json[i],
				sticky : true,
				close : function() {
					if (Object.size(this.notification.actions) == 0 ||
						Object.size(this.notification.actions) == 1) {
						notification_action(this.notification.id, "dismiss");
					}
				}
			});
		}
	}
});

function remove_notification(id)
{
	update_json_file();
}

function notification_action(doc, action)
{
	$.ajax({
		type: 'POST',
		url: '{% url notification_action %}',
		data : {
			"action" : action,
			"id" : doc
		},
		success : function(data, textStatus, XMLHttpRequest) {
			remove_notification(doc);
			if ($("#_notifications").css("display") != "none")
				show_notifications();
			},
		beforeSend : function(XMLHttpRequest, settings) {
			enable_notifications_loader();
		}
	});
}

function refresh_notifications_count()
{
	$( '#notification_count' ).html(notifications_json.length);
}

function update_json_file()
{
	$.ajax({
		type: 'GET', dataType: 'json', url: '{% url notifications %}',
		success : function(data, textStatus, XMLHttpRequest) {
			notifications_json = data.notifications;
			refresh_notifications_count();
			$( "#_notifications_loader" ).hide();
			$( "#_notifications_acc" ).html("");
			get_formatted_notifications().appendTo($( "#_notifications_acc" ));
			$( "#_notifications_acc" ).accordion({
				event : "mouseover"
			});
		},
		beforeSend : function(XMLHttpRequest, settings) {
			enable_notifications_loader();
		}
	});
}

function get_formatted_notifications()
{
	notifications = notifications_json;

	if (notifications.length == 0)
		return $("<p style='text-align:center;'>{% trans "Empty notifications list" %}</p>");

	return $("#notification-template").tmpl(notifications);
}

function enable_notifications_loader()
{
	$( "#_notifications_loader" ).show();	
}

function show_notifications()
{
	if ($("#_notifications").css("display") == "none") {
	var pos = $("#notification_count").position();
		$("#notification_count").css({backgroundColor:"#666"});
		$("#_notifications").css({ display: "block", top: pos.top + 30, left: pos.left});
		update_json_file();
	}
	else {
		$("#notification_count").css({backgroundColor:"transparent"});
		$("#_notifications").css({ display: "none" });
	}
}
