from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden, HttpResponseServerError
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from forms import EditProfileForm

from ufoserver import config
from ufoserver.user import User
from ufo import notify
from ufo.database import DocumentHelper
from ufo.notify import NotificationDocument
from ufo.views import BuddySharesSyncDocument
import ufo.errors

from ipalib.errors import EmptyModlist
import base64
import StringIO
from PIL import Image
import simplejson
from utils import get_notifications, get_quota
import sys
import pwd
import os

def edit(request):
    user = User({ 'apache_env' : { 'KRB5CCNAME' : request.META['KRB5CCNAME'] } },
                request.user.uid)
    user.populate()
    if request.method == "POST":
        form = EditProfileForm(request.POST, request.FILES)
        if form.is_valid():
            if request.FILES.has_key('avatar'):
                avatar = ""
                for chunk in request.FILES['avatar'].chunks():
                    avatar += base64.b64encode(chunk)
                user.set_picture(avatar)
            user.first_name = request.POST['first']
            user.last_name = request.POST['last']
            user.mail = request.POST['email']
            try:
                user.update()
            except EmptyModlist, e:
                pass
    else:
        form = EditProfileForm(
            initial={ "first" : user.first_name,
                      "last" : user.last_name,
                      "email" : user.mail }
        )

    return render_to_response("ufowebprofile/edit.html",
                              { 'form' : form,
                                "username" : request.user.uid,
                                "fullname" : pwd.getpwnam(request.user.uid).pw_gecos,
                                "user_profile" : user,
                                "notifications" : get_notifications(request, request.user.uid),
                                "quota" : get_quota(request, request.user.uid),
                                "results" : [] },
                                context_instance = RequestContext(request))


def notifications(request):
    notifications = get_notifications(request, request.user.uid)
    return HttpResponse(simplejson.dumps({ "notifications" : notifications }),
                        mimetype='application/json')

def notification_action(request):
    if request.method == "POST":
        document = request.POST["id"]
        action = request.POST["action"]
        # TODO: use only one DocumentHelper
        doc_helper = DocumentHelper(NotificationDocument, request.user.uid, config.couchdb_host)
        notification = doc_helper.by_id(key=document, pk=True)

        if action != "dismiss":
            # We create an other DocumentHelper because the first one
            # returns generic (NotificationDocument) objects and we need
            # specialized objects to be able to call the right method

            klass = getattr(notify, notification.subtype + "Notification")
            doc_helper = DocumentHelper(klass, request.user.uid, config.couchdb_host)
            notification = doc_helper.by_id(key=document, pk=True)
            getattr(notification, action).__call__()

        doc_helper.delete(notification)
        return HttpResponse()
             
def buddies(request):
    doc_helper = DocumentHelper(BuddySharesSyncDocument, request.user.uid, config.couchdb_host)
    return HttpResponse(simplejson.dumps([ doc.filename for doc in doc_helper.getDocuments() ]),
                        mimetype='application/json')

def user_to_json(user):
    return { "login" : user.user_name,
             "name" : user.first_name + " " + user.last_name,
             "uid" : user.uidnumber,
             "avatar" : reverse('api.buddies.avatar', args=(user.user_name,)) }

def friend_to_json(friend):
    return { "login" : friend.login,
             "name" : pwd.getpwnam(friend.login).pw_gecos,
             "status" : friend.status,
             "avatar" : reverse('api.buddies.avatar', args=(friend.login,)) }

@login_required(redirect_field_name='redirect_to')
def api_buddies(request, login):
    try:
        if request.method == "GET":
            if not login:
                user = User({ 'apache_env' : { 'KRB5CCNAME' : request.META['KRB5CCNAME'] } }, request.user.uid)
                user.populate()
                all = user.followers.copy()
                all.update(user.followings)
                all.update(user.pending_followers)
                all.update(user.pending_followings)
                return HttpResponse(simplejson.dumps(map(friend_to_json, all.values())),
                                    mimetype='application/json')

            else:
                uid = pwd.getpwnam(login).pw_uid
                user = User({ 'apache_env' : { 'KRB5CCNAME' : request.META['KRB5CCNAME'] } }, login)
                user.populate()
                return HttpResponse(simplejson.dumps(user_to_json(user)), mimetype='application/json')

        elif request.method == "POST":
            if login:
                user = User({ 'apache_env' : { 'KRB5CCNAME' : request.META['KRB5CCNAME'] } }, request.user.uid)
                user.add_pending_follower(login, notify=True)

        elif request.method == "DELETE":
            if login:
                user = User({ 'apache_env' : { 'KRB5CCNAME' : request.META['KRB5CCNAME'] } }, request.user.uid)
                user.remove_follower(login)

        return HttpResponse('{"ok":"true"}')

    except ufo.errors.PendingFollowerError, e:
        return HttpResponseForbidden('{"error":"true","reason":"%s"}' % e.message)

    except Exception, e:
        return HttpResponseServerError('{"error":"true","reason":"%s"}' % e.message)

@login_required(redirect_field_name='redirect_to')
def api_users(request, login):
    try:
        if request.method == "GET":
            q = request.GET.get("q", "")
        if len(q) < 3:
            return HttpResponseServerError('{"error":"true","reason":"query too short"}')
        users = User.find(q)
        return HttpResponse(simplejson.dumps(map(user_to_json, users)), mimetype='application/json')

    except Exception, e:
        return HttpResponseServerError('{"error":"true","reason":"%s"}' % e.message)
            
@login_required(redirect_field_name='redirect_to')
def avatar(request, uid):
    user = User({ 'apache_env' : { 'KRB5CCNAME' : request.META['KRB5CCNAME'] } }, uid)
    w = int(request.GET.get("width", 200))
    h = int(request.GET.get("height", 200))
    picture = user.get_picture()
    response = HttpResponse(mimetype="image/png")
    if picture:
        file = StringIO.StringIO(base64.b64decode(picture))
    else:
        file = open(os.path.join(settings.STATIC_ROOT, "images", "default-avatar.png"), "rb")
    img = Image.open(file)
    img.thumbnail((w, h), Image.BILINEAR)
    img.save(response, format="PNG")
    return response

def logout(request):
    response = HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)
    for cookie in request.COOKIES:
        response.delete_cookie(cookie)
    return response
