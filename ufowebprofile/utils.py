from ufoserver import config
from ufo.database import DocumentHelper
from ufo.notify import NotificationDocument
from ufo.utils import ComponentProxy

from django.shortcuts import render_to_response

import simplejson
import os, sys

import gettext
import functools

def external_dgettext(domain, lang, message):
    os.environ['LANGUAGE'] = lang
    gettext.textdomain(domain)
    return gettext.dgettext(domain, message)
    
def can_fail(func):
    def may_fail(*args, **kw):
        try:
            ret = func(*args, **kw)
            return ret
        except Exception, e:
            import traceback
            print "An exception occurred while executing '%s':" % str(func)
            print sys.exc_info()[0], sys.exc_info()[1]
            traceback.print_tb(sys.exc_info()[2])
            return render_to_response("ufowebfiles/ajaxerror.html", { "exception" : sys.exc_info()[0],
                                                                      "msg" : sys.exc_info()[1],
                                                                      "traceback" : traceback.format_tb(sys.exc_info()[2]) })
    return may_fail

def get_notifications(request, username):
    doc_helper = DocumentHelper(NotificationDocument, username, config.couchdb_host)
    notifications = list(doc_helper.by_id())
    def sort_by_date(n1, n2):
        if n1.date < n2.date: return -1
        elif n1.date == n2.date: return 0
        else: return 1
    notifications.sort(sort_by_date, reverse=True)
    def to_json(notification):
        params = notification._data.get("params", {})
        _ = functools.partial(external_dgettext, "python-ufo", request.LANGUAGE_CODE)
        actions = {}
        for handler in notification.actions:
            actions[handler] = _(notification.actions[handler])
        return { "id" : notification.id,
                 "body" : _(notification.body) % params,
                 "date" : str(notification.date),
                 "title" : _(notification.title) % params,
                 "filepath" : getattr(notification, "filepath", ""),
                 "actions" : actions,
                 "summary" : _(notification.summary) % params,
                 "subtype" : notification.subtype,
                 "following" : notification._data.get("following", "") }
    return map(to_json, notifications)

def get_quota(request, username):
    storage = ComponentProxy("ufostorage.storage.Storage", config.storage_host)
    quota = storage.get_quota(username)
    return { "used" : quota[0] / 1024,
             "percentage" : int(float(quota[0]) / quota[2] * 100) }

