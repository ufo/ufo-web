from django.conf.urls.defaults import *
from django.views.decorators.cache import cache_page
from django.views.generic.simple import direct_to_template

urlpatterns = patterns('ufowebprofile.views',
    url(r'^$', 'edit', name = 'profile'),
    url(r'^js/notifications.js$', cache_page(direct_to_template, 60 * 60 * 24),
                                  { 'template' : 'ufowebprofile/notifications.js', 'mimetype' : 'text/javascript' },
                                  name = 'notifications_js'),
    url(r'^notifications/$', 'notifications', name = 'notifications'),
    url(r'^notifications/action/$', 'notification_action', name = 'notification_action'),

    url(r'^api/buddies/(?P<uid>\w+)/avatar$', 'avatar', name = 'api.buddies.avatar'),
    url(r'^api/buddies/(?P<login>\w*)', 'api_buddies', name = 'api.buddies'),
    url(r'^api/users/(?P<login>\w*)', 'api_users', name = 'api.users'),
)
