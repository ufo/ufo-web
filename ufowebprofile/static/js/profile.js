var buddies = [];

$.extend(dutils.conf.urls, {
	"api.users" : "/profile/api/users/<login>/",
	"api.buddies" : "/profile/api/buddies/<login>/",
	"api.buddies.avatar" : "/profile/api/buddies/<login>/avatar/"
} );

function remove_friend(login)
{
	$.ajax( {
		url : dutils.urls.resolve('api.buddies', { "login" : login }),
		async : true,
		type : 'DELETE',
		success : function (data) {
			notify_success(login + gettext("has been successfully removed from your friend list"));
			update_friends('friendsgrid');
		}
	} );
}

function add_friend(login)
{
	$.ajax( {
		url : dutils.urls.resolve('api.buddies', { "login" : login }),
		async : true,
		type : 'POST',
		success : function (data) {
			notify_success(gettext("A request has been sent to") + " " + login);
		}
	} );
}

function display_friends(div, data)
{
	$('#' + div).html("");
	$("#user-template").tmpl(data)
		.appendTo('#' + div);
}

function update_friends(div)
{
	$.ajax( {
		url : dutils.urls.resolve('api.buddies', { "login" : "" }),
		async : true,
		success : function (data) {
			buddies = data;
			display_friends(div, data);
		}
	});
}
