#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import glob

from distutils.core import setup

setup(
    name="ufo-web",
    version="0.1",
    description="The UFO Web infrastructure",
    author="Sylvain Baubeau",
    author_email="sylvain.baubeau@agorabox.org",
    url="http://www.agorabox.org/",
    license="GPLv2",
    packages=["ufoweb", "ufoweb.templatetags", "ufowebprofile"],
    package_data = { 'ufoweb' : [ 'static/js/*.js',
                                  'static/css/start/images/*.png',
                                  'static/css/start/*.css',
                                  'static/css/*.css',
                                  'static/images/*.gif',
                                  'static/images/*.png',
                                  'static/images/base/*.png',
                                  'static/images/flags/*.png',
                                  'static/images/notifications/*.gif',
                                  'static/images/notifications/*.png',
                                  'templates/*.html',
                                  'locale/fr/LC_MESSAGES/django.mo' ],
                     'ufowebprofile' : [ 'static/js/*.js',
                                         'static/css/*.css',
                                         'static/images/*.png',
                                         'templates/ufowebprofile/*.html',
                                         'templates/ufowebprofile/*.js',
                                         'locale/fr/LC_MESSAGES/django.mo' ] },
)
