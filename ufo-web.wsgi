import os, sys
import django.core.handlers.wsgi

# Make sure the Cookie Python module is patched to accept '/' and '@'
# as the following lines do not work because of default parameters
# Cookie._LegalChars += "@/"
import Cookie
assert "@" in Cookie._LegalChars and "/" in Cookie._LegalChars

# Set up logging

# import logging
# import sys
# logger = logging.getLogger('')
# logger.setLevel(logging.DEBUG)
# handler = logging.StreamHandler(sys.stderr)
# handler.setLevel(logging.DEBUG)
# formatter = logging.Formatter('%(levelname)-8s %(message)s')
# handler.setFormatter(formatter)
# logger.addHandler(handler)

os.environ["DJANGO_SETTINGS_MODULE"] = "ufoweb.settings"
application = django.core.handlers.wsgi.WSGIHandler()

