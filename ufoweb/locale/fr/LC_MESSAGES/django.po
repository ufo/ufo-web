# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-01-25 19:58+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: templates/404.html:9
msgid "The page you requested cannot be found"
msgstr "La page recherchée est introuvable"

#: templates/404.html:12
msgid "If you typed the page url, check the spelling."
msgstr ""
"Si vous avez entré l'adresse manuellement, vérifier que celle ci est correcte"

#: templates/404.html:13
msgid ""
"Start from the <a href=\"http://www.agorabox.org\">Agorabox Home Page</a>, "
"and look for links to the information you want."
msgstr ""
"Cherchez le lien recherché à partir du <a href=\"http://www.agorabox.org"
"\">Agorabox Home Page</a>site Web Agorabox</a>"

#: templates/404.html:14
msgid ""
"Click the <a onclick=\"javascript:history.back(1); return false;\">Back</a> "
"button to try another link."
msgstr ""
"Cliquez sur <a onclick=\"javascript:history.back(1); return false;"
"\">'Retour'</a> et essayez un autre lien."

#: templates/500.html:9
msgid "An error has occured"
msgstr "Une erreur est survenue"

#: templates/500.html:11
msgid ""
"Something went wrong. We are sorry for the inconvenience. Try again later or "
"let us now about the problem with <a href='http://www.agorabox.org/"
"contact'>contacting us</a>"
msgstr ""
"Une erreur est survenue. Veuillez nous excuser pour le désagrément. Veuillez "
"réessayer plus tard ou nous <a href='http://www.agorabox.org/"
"contact'>signaler le problème"

#: templates/base.html:108
msgid "Mo Used"
msgstr "Mo utilisés"

#: templates/base.html:109
msgid "Logout"
msgstr "Se déconnecter"

#: templates/base.html:121
msgid "My files"
msgstr "Mes fichiers"

#: templates/base.html:122
msgid "My notes"
msgstr "Mes notes"

#: templates/base.html:123
msgid "My account"
msgstr "Mon compte"

#: templates/base.html:154
msgid "Dismiss"
msgstr "Ok"
