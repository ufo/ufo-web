import os.path
import glob
import site

SITE_PACKAGES = site.getsitepackages()[0]

conf_files_path = "/etc/ufo-web/*.conf"
conffiles = glob.glob(conf_files_path)
conffiles.sort()

for f in conffiles:
    execfile(os.path.abspath(f))
