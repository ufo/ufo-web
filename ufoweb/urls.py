from django.conf.urls.defaults import *
from settings import MEDIA_ROOT

from django.contrib import admin
import ufowebadmin
admin.autodiscover()

urlpatterns = patterns('',
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', { "document_root" : MEDIA_ROOT } ),
    (r'^account/logout', 'ufowebprofile.views.logout'),
    (r'^account/', include('registration.urls')),
    (r'^logout/', 'ufowebprofile.views.logout'),
    (r'^admin/', include(admin.site.urls)),
    (r'^files/', include('ufowebfiles.urls')),
    (r'^profile/', include('ufowebprofile.urls')),
    (r'^notes/', include('notes.urls')),
    (r'^jsi18n/(?P<packages>\S+?)/$', 'django.views.i18n.javascript_catalog'),
)
